/*
	hercules.cpp - Main program definition for Hercules, includes daemonization logic as well
					as basic signal handling and taredown logic

	"If you think this has a happy ending, you haven't been paying attention."
*/

#include <cstdio>
#include <cstdlib>
#include <unistd.h>
#include <fcntl.h>
#include <csignal>
#include <syslog.h>
#include <cerrno>
#include <pwd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>

#include <thread>
#include <chrono>

#include "./include/global.hpp"
#include "./include/stack_manager.hpp"


#define DAEMON_NAME 			"Hercules"

#define DEFAULT_RUN_AS_USER 	"nobody"
#define DEFAULT_LOCK_FILE 		"hercules.lock"
#define DEFAULT_LOCK_DIR		"/var/lock/subsys/"
#define DEFAULT_WORKING_DIR		"/"

using namespace Hercules;

stack_manager * ptrStkMgr = nullptr;

bool daemonizing = true;
int lfp = nullptr;

static void sighandler(int signid);
static void daemonize(const char * lockfile);
static void teardown(int exitstatus);


/*
	main - Main program entry point

	[IN] int argc 		- Argument count
	[IN] char * argv[]	- Argument value array
	[IN] char * env[]	- Environment Array
	[OUT] int 			- Exit status of the process
*/
int main(int argc, char* argv[], char* env[]) {
	/* Initialize logging interface */
	openlog(DAEMON_NAME, LOG_PID, LOG_LOCAL5);
	syslog(LOG_INFO, "Starting..");

	/* Process command line arguments */

	/* Load Configuration File */

	/* Daemonize Process */
	daemonize(DEFAULT_LOCK_DIR DEFAULT_LOCK_FILE);

	/* Once deamonized, complete setup and spawn stack manager */
	stack_manager stkmgr();
	ptrStkMgr = &stkmgr;
	/* Spin up stack threads */

	NetworkStack netStack;
	GFSStack gfsStack;
	JobStack jobStack;

	ptrStkMgr->SpinUpStack(&netStack); /* Network stack must always spin up first */
	ptrStkMgr->SpinUpStack(&gfsStack); /* GFS Second, this allows for everything to sync */
	ptrStkMgr->SpinUpStack(&jobStack); /* Lastly the Job Stack */

	/* teardown once done */
	teardown(EXIT_SUCCESS);
	/* Tidy up */
	syslog(LOG_INFO, "Releasing lock file %s", DEFAULT_LOCK_DIR DEFAULT_LOCK_FILE)
	close(lfp);

	syslog(LOG_NOTICE, "Terminated");
	closelog();

	return EXIT_SUCCESS;
}

/*
	sighandler - Process and catch signals from the operating system and other processes

	[IN] int sigid - the signal ID
*/
static void sighandler(int signid) {
	switch(sigid) {
		case SIGALRM: {
			syslog(LOG_ERR, "Alarm signal caught, shutting down");
			teardown(EXIT_FAILURE);
			exit(EXIT_FAILURE);
			break;
		}
		case SIGTERM: {
			syslog(LOG_INFO, "Terminate signal caught, shutting down");
			teardown(EXIT_SUCCESS);
			exit(EXIT_SUCCESS);
			break;
		}
		case SIGHUP: {
			syslog(LOG_INFO, "Hangup signal caught, disabling network stack");
			/* disable network stack */
			ptrStkMgr->spinDownStackByName("network");
			break;
		}
		case SIGCHLD: {
			syslog(LOG_ERR, "SIGCHLD caught, shutting down");
			teardown(EXIT_FAILURE);
			exit(EXIT_FAILURE);
			break;
		}
		case SIGUSR1: {
			syslog(LOG_INFO, "SIGUSR1 caught, die");
			exit(EXIT_FAILURE);
			break;
		}
	}
}

/*
	daemonize - Daemonizes the process as to detach from the current shell

	[IN] const char * lockfile - The lock file to use when daemonizing
*/
static void daemonize(const char * lockfile) {
	pid_t pid, sid, parent;

	/* If we are already a daemon then no need to do the rest, so return */
	if(getppid() == 1) return;

	/* Create lock file as the current user */
	if(lockfile  && lockfile[0]) {
		lfp = open(lockfile, O_RDWR | O_CREAT, 0640);
		if(lfp < 0) {
			syslog(LOG_ERR, "Unable to create lock file %s, error code: %d (%s)", lockfile, errno, strerror(errno));
			exit(EXIT_FAILURE);
		}
	}

	/* drop user if there is one and then run as RUN_AS_USER */
	if(getuid() == 0 || geteuid() == 0) {
		struct passwd *pw = getpwnam(RUN_AS_USER);
		if(pw) {
			syslog(LOG_NOTICE, "Setting user to " RUN_AS_USER);
			setuid(pw->pw_uid);
		}
	}

	/* Register signal handler */
	signal(SIGALRM, sighandler);
	signal(SIGTERM, sighandler);
	signal(SIGAHUP, sighandler);
	signal(SIGACHLD, sighandler);
	signal(SIGUSR1, sighandler);

	/* Fork off parent process */
	pid = fork();
	if(pid < 0) {
		syslog(LOG_ERR, "Unable to fork daemon, error code: %d (%s)", errno, strerror(errno));
		exit(EXIT_FAILURE);
	}

	/* check if all is well, if so, we kill off the parent process */
	if(pid > 0) {
		/* Wait for SIGTERM or SIGCHLD to confirm, or wait two seconds and SIGALRM  */
		alarm(2);
		pause();
		exit(EXIT_FAILURE);
	}

	/*  Get the parent process ID */
	parent = getppid();

	/* Cancel certain signals */
    signal(SIGCHLD,SIG_DFL); /* A child process dies */
    signal(SIGTSTP,SIG_IGN); /* Various TTY signals */
    signal(SIGTTOU,SIG_IGN);
    signal(SIGTTIN,SIG_IGN);
    signal(SIGHUP, SIG_IGN); /* Ignore hangup signal */
    signal(SIGTERM,SIG_DFL); /* Die on SIGTERM */

    /* change the file mode mask */
    umask(0);

    /* Create a new SID for the child process */
    sid = setsid();
    if(sid < 0) {
    	syslog(LOG_ERR, "Unable to create new session, error code: %d (%s)", errno, strerror(errno));
    	exit(EXIT_FAILURE);
    }

    /* change working directory, parents from current directory from locking */
    if(chdir(DEFAULT_WORKING_DIR) < 0) {
    	syslog(LOG_ERR, "Unable to change working directory to %s, error code: %d (%s)", DEFAULT_WORKING_DIR, errno, strerror(errno));
    	exit(EXIT_FAILURE);
    }

    /* Redirect standard output streams to /dev/null */
    freopen("/dev/null", "r", stdin);
    freopen("/dev/null", "w", stdout);
    freopen("/dev/null", "w", stderr);

    /* Kill the parent process, muahahahaha */
    kill(parent, SIGUSR1);
    daemonizing = false;
}

/*
	teardown - reshuffles jobs, wraps up data and stores it in the GFS, kills jobs, sends out
			   NODE_TRDWN and NODE_EXIT to job server spins down stacks and prepares for exit

	[IN] int exitstatus - the status that called for the exit, 0 for normal exit 1 for abnormal exit
*/
static void teardown(int exitstatus) {
	if(!daemonizing) {
		/* If we are not daemonizing then we do things, if not, we are not set up quit yet, so no need */
		ptrStkMgr->spinDownStacks(exitstatus);
	}

}
