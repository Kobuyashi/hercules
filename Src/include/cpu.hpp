/*
	cpu.hpp - Defines and such for CPU architectures and assembly bits, as well some other things
*/

#if __sparc__
#	define CPU_ARCH			"SPARC"
#	define CPU_FAMILY		"SPARC"
#elif __powerpc__
#	define CPU_ARCH			"PowerPC"
#	define CPU_FAMILY		"PowerPC"
#elif __ia64__ || _M_IA64
#	define CPU_ARCH			"IA-64"
#	define CPU_FAMILY		"Itanium"
#elif __i386__
#	define CPU_FAMILY		"ix86"
#	if __i386__
#		define CPU_ARCH		"i386"
#	elif __i486__
#		define CPU_ARCH		"i486"
#	elif __i586__
#		define CPU_ARCH		"i586"
#	elif __i686__
#		define CPU_ARCH		"i686"
#	else
#		error "Unknown CPU Architecture, unable to compile assembly"
#	endif
#elif __amd64__
#	define CPU_ARCH			"AMD64"
#	define CPU_FAMILY		"AMD"
#else
#	error "Unknown CPU Architecture, can not use unable to compile assembly"
#endif
