/*
	global.hpp - Global defines and compiler tests
*/
#pragma once
#if !defined(_Hercules_global_hpp_)
#	define _Hercules_global_hpp_

#	include "./cpu.hpp"
#	define EXIT_SUCCESS 			0
#	define EXIT_FAILURE 			1
#	if _MSC_VER
#		error "No. Bad. No MSVC++"
#	endif
#	if !defined(__linux__) || !defined(__linux) || !defined(__unix__) || !defined(__unix)
#		error "Non Linux or UNIX system! STOP."
#	endif

#	if defined(__clang__)
#		define COMPILER __clang_version__
#		define __func__ __PRETTY_FUNCTION__
#	elif defined(__GNUG__)
#		define GCC_VERSION (__GNUC__ * 10000 + __GNUC_MINOR__ * 100 + __GNUC_PATCHLEVEL__)
#		if GCC_VERSION < 40800
#			error ""
#		endif
#		define COMPILER "GCC " __VERSION__
#		define __func__ __PRETTY_FUNCTION__
#	else
#		error "Use of clang or G++ required ."
#	endif

// Lets us make C strings out of almost anything
#	define STRINGIZE(x) STRINGIZE2(x)
#	define STRINGIZE2(x) #x

#	define __LINE_S__ STRINGIZE(__LINE__)

// Allows for us to deprecate an API
#	define _DEPRECATED __attribute__((deprecated))
#	define _DEPRECATED(X) __attribute__((deprecated(X)))

#	define HERCULES_VERSION_MAJOR "0"
#	define HERCULES_VERSION_MINOR "1"
#	define HERCULES_VERSION_RELEASE "0"
#	define HERCULES_VERSION_BUILD	"2"

#	define HERCULES_VERSION "HERCULES " HERCULES_VERSION_MAJOR "." HERCULES_VERSION_MINOR
#	define HERCULES_RELEASE HERCULES_VERSION "." HERCULES_VERSION_RELEASE " Build " HERCULES_VERSION_BUILD
#	define HERCULES_COPYRIGHT HERCULES_RELEASE " Copyright (C) 2014"

#endif /* _Hercules_global_hpp_ */
