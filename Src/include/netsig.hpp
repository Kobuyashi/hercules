/*

*/
#pragma once
#ifndef _Hercules_Network_Signals_hpp_

#define NODE_SPAWN	0x1024
#define NODE_EXIT	0x1025
#define NODE_FAULT	0x1026
#define NODE_TRDWN	0x1027

#define GFS_STAT	0x2048
#define GFS_STOR	0x2049
#define GFS_RETV	0x2050

#endif /* _Hercules_Network_Signals_hpp_ */