/*
	stack_base.hpp - Base abstract class for hercules stacks
*/
#pragma once
#ifndef _Hercules_Stack_Base_hpp_
namespace Hercules {
	class stack_base {
	public:
		virtual void stackThreadMain(void* params) = 0;
	};
}
#endif /* _Hercules_Stack_Base_hpp_ */