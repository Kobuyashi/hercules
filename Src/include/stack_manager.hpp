/*
	stack_manager.hpp - Header for the Hercules stack manager
*/

#include "stack_base.hpp"

#pragma once
#ifndef _Hercules_Stack_Manager_hpp_
#define _Hercules_Stack_Manager_hpp_
namespace Hercules {
	class stack_manager	{
	private:
		
		stack_base * stackPtrList[3];
	
		/*
			spinDownStackByPtr - Spins down a stack by the stack pointer
		*/
		void spinDownStackByPtr(stack_base * stackPtr) const;
	public:
		/*
			stack_manager - Core for managing hercules stacks
		*/
		stack_manager(void);
		~stack_manager(void);

		void spinUpStack(const stack_base * stackPtr) const;

		/*
			spinDownStacks - Shuts down all stacks

			[IN] int exitStatus - The exit code used in order to determine shutdown mode
		*/
		void spinDownStacks(int exitStatus);

		/*
			spinDownStackByName - shuts down the stack with the given name

			[IN] const char * stackName - The name of the stack to spin down
		*/
		void spinDownStackByName(const char * stackName);

	};
}
#endif /* _Hercules_Stack_Manager_hpp_ */